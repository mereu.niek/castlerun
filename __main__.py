import ast
import argparse

from castle import Castle
from solver import CastleSolver
from validation import validate_grid

def main(w: int, h: int, grid: list = None) -> None:
    """
    Main function to solve the castle game.

    Parameters
    ----------
    w : int
        Width of the castle.
    h : int
        Height of the castle.
    grid : list, optional
        A list of lists representing the grid, by default None
    """
    # validate the grid object
    if grid is not None and not validate_grid(grid, w, h):
        print("Invalid grid")
        return
    
    if grid is None:
        castle = Castle(w, h)
    else:
        castle = Castle(w, h, grid)

    castle.print_grid()
    solver = CastleSolver(castle)
    solver.solve()

    if solver.path_length == 0:
        print("Impossible")
    else:
        print(solver.path_length)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Solve a castle with given width and height.")
    parser.add_argument("width", type=int, help="Width of the castle.")
    parser.add_argument("height", type=int, help="Height of the castle.")
    parser.add_argument("--grid", type=str, help="Grid to play in.", default=None)

    args = parser.parse_args()

    grid = ast.literal_eval(args.grid) if args.grid is not None else None
    
    main(args.width, args.height, grid)
