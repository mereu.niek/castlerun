from collections import deque
from typing import List, Dict, Tuple, Optional
from castle import Castle

class CastleSolver:
    """A class representing a solver for the castle game.""" 

    def __init__(self, castle: Castle):
        """
        Initialize the solver.

        Parameters
        ----------
        castle : Castle
            The castle to solve.
        """
        self.castle = castle
        self.player = castle.player
        self.monster = castle.monster
        self.path_length: Optional[int] = None

    def solve(self) -> Optional[List[Tuple[int, int]]]:
        """
        Solve the castle game.

        Returns
        -------
        list
            A list of tuples representing the path from the player to the exit.
        """
        # use a deque object to store the positions to process
        queue: deque = deque([(self.player.x, self.player.y)])  
        # The dictionary to keep track of the path
        prev: Dict[Tuple[int, int], Optional[Tuple[int, int]]] = {(self.player.x, self.player.y): None} 

        # While there are positions to process
        while queue:
            x, y = queue.popleft()  # Get the position at the front of the queue

            # If this is the exit, return the path
            if self.castle.grid[y][x] == "E":
                path = self.get_path(prev, x, y)
                self.path_length = len(path) - 1
                return path

            # Check all neighboring cells
            for dx, dy in [(-1, 0), (0, -1), (1, 0), (0, 1)]:
                next_x, next_y = x + dx, y + dy

                # If we can move to the cell and haven't visited it before, add it to the queue
                if self.can_move(next_x, next_y) and (next_x, next_y) not in prev:
                    queue.append((next_x, next_y))
                    prev[next_x, next_y] = x, y

        self.path_length = 0
        return None

    def can_move(self, x: int, y: int) -> bool:
        """
        Check if a position is in the grid and is not a wall.

        Parameters
        ----------
        x : int
            The x-coordinate of the position.
        y : int
            The y-coordinate of the position.

        Returns
        -------
        bool
            True if the position is in the grid and is not a wall, False otherwise.
        """
        # Position must be in grid and not in a wall
        if not (0 <= x < len(self.castle.grid[0]) and 0 <= y < len(self.castle.grid) and
                self.castle.grid[y][x] != "#"):
            return False

        # Simulate the monster's movement
        monster_next_x, monster_next_y = self.monster.x, self.monster.y
        if self.monster.x != x:  # If the horizontal coordinates are not the same, move closer horizontally
            if self.monster.x < x and self.castle.grid[self.monster.y][self.monster.x + 1] != "#":
                monster_next_x += 1
            elif self.monster.x > x and self.castle.grid[self.monster.y][self.monster.x - 1] != "#":
                monster_next_x -= 1
        if self.monster.y != y:  # If the vertical coordinates are not the same, move closer vertically
            if self.monster.y < y and self.castle.grid[self.monster.y + 1][self.monster.x] != "#":
                monster_next_y += 1
            elif self.monster.y > y and self.castle.grid[self.monster.y - 1][self.monster.x] != "#":
                monster_next_y -= 1
                # Check if the monster's next position is the same as the player's next position
        return (x, y) != (monster_next_x, monster_next_y)

    def get_path(self, prev: Dict[Tuple[int, int], Tuple[int, int]], x: int, y: int) -> List[Tuple[int, int]]:
        """
        Build the path from the exit to the start by following the links in prev.

        Parameters
        ----------
        prev : dict
            The dictionary to keep track of the path.
        x : int
            The x-coordinate of the exit.
        y : int
            The y-coordinate of the exit.

        Returns
        -------
        list
            A list of tuples representing the path from the player to the exit.
        """
        # Build the path from the exit to the start by following the links in prev
        path: List[Tuple[int, int]] = [(x, y)]
        while prev[x, y] is not None:
            x, y = prev[x, y]
            path.append((x, y))
        path.reverse()  # Reverse the path
        return path
