import random

from typing import List, Optional, Tuple
class Castle:
    """ A class representing a castle. """
    def __init__(self, w: int, h: int, grid: Optional[List[List[str]]] = None):
        """
        Initialize the grid.

        Parameters
        ----------
        w : int
            The width of the grid.
        h : int
            The height of the grid.
        grid : list, optional
            A list of lists representing the grid. Default is None.
        """
        if grid is None:
            self.grid, player_pos, monster_pos = self.generate_grid(w, h)
        else:
            self.grid = grid
            player_pos = self.find_position("P")
            monster_pos = self.find_position("M")

        self.player = Player(*player_pos, self)
        self.monster = Monster(*monster_pos, self)

    def generate_grid(self, w: int, h: int) -> Tuple[List[List[str]], Tuple[int, int], Tuple[int, int]]:
        """
        Generate a random grid.

        Parameters
        ----------
        w : int
            The width of the grid.
        h : int
            The height of the grid.

        Returns
        -------
        grid : list
            A list of lists representing the grid.
        player_pos : tuple
            The coordinates of the player.
        monster_pos : tuple
            The coordinates of the monster.
        """
        # Create a grid filled with pathways
        grid = [["." for _ in range(w)] for _ in range(h)]

        # Place walls, player, monster, and exit at random locations
        for _ in range(int(w * h * 0.2)):  # Fill about 20% of the grid with walls
            x, y = random.randint(0, w - 1), random.randint(0, h - 1)
            grid[y][x] = "#"

        # initialize locations for E, M, and P on 0,0
        px, py = 0, 0  
        mx, my = 0, 0
        ex, ey = 0, 0

        # Ensure P, M, and E have unique positions
        while (px, py) == (mx, my) or (px, py) == (ex, ey) or (mx, my) == (ex, ey):
            px, py = random.randint(0, w - 1), random.randint(0, h - 1)
            mx, my = random.randint(0, w - 1), random.randint(0, h - 1)
            ex, ey = random.randint(0, w - 1), random.randint(0, h - 1)

        grid[py][px] = "P"
        grid[my][mx] = "M"
        grid[ey][ex] = "E"

        return grid, (px, py), (mx, my)
    
    def find_position(self, symbol: str) -> Tuple[Optional[int], Optional[int]]:
        """
        Find the position of a symbol in the grid.

        Parameters
        ----------
        symbol : str
            The symbol to find.

        Returns
        -------
        pos : tuple
            The coordinates of the symbol.
        """
        for y, row in enumerate(self.grid):
            for x, cell in enumerate(row):
                if cell == symbol:
                    return x, y
        return None, None

    def print_grid(self) -> None:
        """Print the grid."""
        for row in self.grid:
            print("".join(row))


class Player:
    """ A class representing a player. """
    def __init__(self, x: int, y: int, grid: List[List[str]]):
        """
        Initialize the player.

        Parameters
        ----------
        x : int
            The x-coordinate of the player.
        y : int
            The y-coordinate of the player.
        grid : list
            A list of lists representing the grid.
        """
        self.x = x
        self.y = y
        self.grid = grid


class Monster:
    """A class representing a monster. """
    def __init__(self, x: int, y: int, grid: List[List[str]]):
        """
        Initialize the monster.

        Parameters
        ----------
        x : int
            The x-coordinate of the monster.
        y : int
            The y-coordinate of the monster.
        grid : list
            A list of lists representing the grid.
        """
        self.x = x
        self.y = y
        self.grid = grid
