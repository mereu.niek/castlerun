# CastleRun 

Welcome to My Castle Game! This is a simple maze-solving game where you need to find your way to the exit without getting caught by the monster. The game is solved using the BFS (Breadth-First Search) algorithm to find the shortest path from the player to the exit.


## Local Environment

To set up a local environment with Python 3.9, follow the instructions below:

1. Make sure you have Conda installed. If you don't have Conda, you can install Miniconda or Anaconda, which provide Conda package management system.

2. Clone the repository to your local machine:

   ```bash
   git clone https://github.com/your-username/your-repository.git
   ```

3. Navigate to the project directory:
    ```bash
    cd castlerun
    ```

4. Create the Conda environment using the provided environment.yml file:
```bash
conda env create -f environment.yml
```

5. Activate the Conda environment:

```bash
conda activate CastleRun
```


## Running the Game

### Play with random grid

To play the game with a random grid, use the following command:

```bash
python __main__.py 10 10
```

where 10 and 10 are the width and height of the grid, respectively.

### Play with a custom grid

To play the game with a random grid, use the following command:

```bash
python __main__.py 7 7 --grid "[['.', '#', '.', '.', '.', '.', '.'], ['.', '#', '.', 'P', '#', '#', '.'], ['.', '#', '.', '#', '.', '.', '.'], ['.', '.', '.', '#', '.', '#', '.'], ['.', '#', '#', '#', '#', '#', '.'], ['.', '.', '.', '.', '.', 'M', '.'], ['.', '#', '#', '#', '#', '#', 'E']]"
```

where the grid is represented as a nested list.

