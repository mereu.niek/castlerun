"""This module contains validation functions for the castle game."""

def validate_grid(grid: list, w: int, h: int) -> bool:
    """
    Check if the grid contains "E", "P", and "M" exactly once each.

    Parameters
    ----------
    grid : list
        A list of lists representing the grid.
    w : int
        Width of the grid.
    h : int
        Height of the grid.

    Returns
    -------
    bool
        True if the grid is valid, False otherwise.
    """
    symbols = {"E", "P", "M"}
    found_symbols = set()

    if not (2 <= w) or not (h <= 50):
        return False

     # Check grid dimensions
    if len(grid) != h or any(len(row) != w for row in grid):
        return False

    for row in grid:
        for cell in row:
            if cell in symbols:
                if cell in found_symbols:  # Check for duplicates
                    return False
                found_symbols.add(cell)
            elif cell not in {".", "#"}:  # Check if cell contains other than ".", "#", "E", "P", "M"
                return False
    
    return found_symbols == symbols  # Check if all symbols are found
